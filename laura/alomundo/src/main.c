
#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

#include "Timer.h"
#include "BlinkLed.h"
// coisas das tarefa
int add(int x,int y);
unsigned long fatorial(unsigned short n);
unsigned long fibonacci(unsigned short n);
void soma(int *p1, int *p2, int n);
void setbit(unsigned long *p, unsigned char bitn);
void resetbit(unsigned long *p, unsigned char bitn);
void togglebit(unsigned long *p, unsigned char bitn);
unsigned char readbit(unsigned long *p, unsigned char bitn);
unsigned long readbitslice(unsigned long *p, unsigned char bitstart, unsigned char bitend);
unsigned char islittleendian(void);
void * mymemset ( void * ptr, int value, size_t num );
void * mymemcpy ( void * destination, const void * source, size_t num );
int mystrcmp ( const char * str1, const char * str2 );
size_t mystrlen(const char *str);

// Testes
void memcpyTeste(void);
void mymemsetTeste();
void bitTest();
void endianTeste();
void fiboTeste();
void fatorialTeste();
void somaTeste();
void strcmpTeste();
void strcpyTeste();

// Keep the LED on for 2/3 of a second.
#define BLINK_ON_TICKS  (TIMER_FREQUENCY_HZ * 3 / 4)
#define BLINK_OFF_TICKS (TIMER_FREQUENCY_HZ - BLINK_ON_TICKS)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


// para teste do mymemcpy
struct {
  char name[40];
  int age;
} person, person_copy;

int
main(int argc, char* argv[])
{
//	int x;
//	x=add(2,3);
//	trace_printf("2+3=%d\n", x);
//
	memcpyTeste());
	mymemsetTeste();
	bitTest();
	endianTeste();
	fiboTeste();
	fatorialTeste();
	somaTeste();
//  // Send a greeting to the trace device (skipped on Release).
//  trace_puts("Hello ARM World!");
//
//  // At this stage the system clock should have already been configured
//  // at high speed.
//  trace_printf("System clock: %u Hz\n", SystemCoreClock);
//
//  timer_start();
//
//  blink_led_init();
//
//  uint32_t seconds = 0;
//
//  // Infinite loop
//  while (1)
//    {
//      blink_led_on();
//      timer_sleep(seconds == 0 ? TIMER_FREQUENCY_HZ : BLINK_ON_TICKS);
//
//      blink_led_off();
//      timer_sleep(BLINK_OFF_TICKS);
//
//      ++seconds;
//
//      // Count seconds on the trace device.
//      trace_printf("Second %u\n", seconds);
//    }
//  // Infinite loop, never return.
}

void memcpyTeste(void){
	char myname[] = "TOpster";
	mymemcpy ( person.name, myname, strlen(myname)+1 );
	person.age = 129;

	mymemcpy ( &person_copy, &person, sizeof(person) );

	trace_printf("person_copy: %s, %d \n", person_copy.name, person_copy.age );
}

void mymemsetTeste(){
	char top[] = "oi, eu sou a laura";
	mymemset(top, 'q', 4);
	trace_printf("novo: %s\n", top);
	memcpyTeste();
}

void bitTest(){
	int e = 0;
	setbit(&e, 10);
	trace_printf("e com 10 setado e %i\n", e);
	resetbit(&e,10);
	trace_printf("%i\n", e);
	trace_printf("e com 10 resetado e %i\n", e);
	trace_printf("toggle 10");
	togglebit(&e, 10);
	trace_printf("e: %i\n", e);
	trace_printf("toggle 10 novamente");
	togglebit(&e, 10);
	trace_printf("e: %i\n", e);
	unsigned char f;
	f = readbit(&e,10);
	trace_printf("e10: %u\n", f);
	f = readbit(&e,9);
	trace_printf("e9: %u\n", f);
	f = readbit(&e,8);
	trace_printf("e8: %u\n", f);
	readbitslice(&e,1,12);
	unsigned long g;
	g = readbitslice(&e,1,12);
	trace_printf("g eh %lu\n", g);
	g = readbitslice(&e,1,3);
	trace_printf("g eh %lu\n", g);
}

void endianTeste(){
	unsigned char f;
	f = islittleendian();
	trace_printf("f eh %u\n", f);
	if(f==0){
		trace_printf("eh big endian\n");
	}
	else{
		trace_printf("eh little endian\n");
	}
}

void fiboTeste(){
	int fib;
	fib = fibonacci(1);
	trace_printf("fib eh %lu\n", fib);
	fib = fibonacci(2);
	trace_printf("fib eh %lu\n", fib);
	fib = fibonacci(3);
	trace_printf("fib eh %lu\n", fib);
	fib = fibonacci(4);
	trace_printf("fib eh %lu\n", fib);
	fib = fibonacci(5);
	trace_printf("fib eh %lu\n", fib);
	fib = fibonacci(6);
	trace_printf("fib eh %lu\n", fib);
}

void fatorialTeste(){
	int fat;
	fat = fatorial(1);
	trace_printf("fat eh %lu\n", fat);
	fat = fatorial(2);
	trace_printf("fat eh %lu\n", fat);
	fat = fatorial(3);
	trace_printf("fat eh %lu\n", fat);
	fat = fatorial(4);
	trace_printf("fat eh %lu\n", fat);
	fat = fatorial(10);
	trace_printf("fat eh %lu\n", fat);
}

void somaTeste(){
	int a[4] ={2,3,4,5};
	int b[4] = {1,1,1,1};
	soma(a,b,4);
	int i;
	for (i=0;i<4;i++){
		trace_printf("%i\n",a[i]);
	}
	int c[5] ={7,7,7,7,7};
	int d[5] = {10,20,30,40,50};
	soma(c,d,5);
	for (i=0;i<5;i++){
		trace_printf("%i\n",c[i]);
	}
}

void strcmpTeste(){
	char a[] = "aaab";
	char b[] = "aaac";
	int c = mystrcmp(a,b);
	if(c<0){
		trace_printf("b");
	}
	if(c==0){
		trace_printf("iguais");
	}
	if(c>0){
		trace_printf("a");
	}
}

void strcpyTeste(){
	char a[] = "aaaaaaaa";
	char b[] = "bbbbbbbb";
	int comp =  mystrcmp(a, b);
	trace_printf("novo: %d\n", comp);
}

void strlenTeste(){
	a[] = "12345";
	int n = mystrlen(a);
	trace_printf("tamanho %i", n);
}

#pragma GCC diagnostic pop

